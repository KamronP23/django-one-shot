# Generated by Django 4.1.3 on 2022-12-01 21:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="todoitem",
            old_name="todos",
            new_name="list",
        ),
    ]
