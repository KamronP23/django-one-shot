from django.urls import path
from todos.views import todo_list_list, todo_item_list, create_todo_list, todo_list_update, todo_list_delete


urlpatterns = [
    path("", todo_list_list, name="todos"),
    path("todos/<int:id>/", todo_item_list, name="todo_list_detail"),
    path("create/", create_todo_list, name="create"),
    path("<int:id>/edit/", todo_list_update, name="edit"),
    path("<int:id>/delete/", todo_list_delete, name="delete"),
]
