from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm
# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    print("this is todo_list_list", todo_list_list)
    print("this is todos", todos)
    return render(request, "todos/list.html", context)


def todo_item_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "item_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=todo)
        if form.is_valid():
            todo = form.save()
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ItemForm(instance=todo)
        context = {
            "form": form,
            "item": todo,
        }
        return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todos")
    return render(request, "todos/delete.html")
